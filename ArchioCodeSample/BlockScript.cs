﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class BlockScript : MonoBehaviour
{
	
	public bool toMove = false;
    public bool toCombine = false;
    public bool toCombineAnchor = false;

    public float MoveForce = 5;
    public float VelocityCap = 5;
    float MoveAccuracy = 0.5f;

    public Vector3 Destination;

	public bool contact = false;
	Vector3 contactNormal = Vector3.zero;

	public GameObject CombineTarget;

	public GameObject AnchorTarget;

	public Vector3 AnchorNormal;

	ConstantForce cf;

	Vector3 AnchorPoint;

	Rigidbody rb;

	public AlienTextBubble TextBubble;

	public enum BasicBlockType
	{
		connector,
		power,
		data,
		emitter,
		guide,
	}

	public int PowerVal = 0;

	public int DataVal = 0;

	public int EmitterVal = 0;

	public BasicBlockType BlockType;

	public bool CompositeBlock = false;

	public bool LevelBlock = false;

	List<Quaternion> AcceptableRotations = new List<Quaternion> ();

	public AudioSource BlockSound;

	void Start ()
	{
		Destination = Vector3.zero;
		
		cf = gameObject.GetComponent<ConstantForce> ();
		
		rb = gameObject.GetComponent<Rigidbody> ();

		TextBubble = gameObject.GetComponentInChildren<AlienTextBubble> ();

        AcceptableRotations = Controller.controller.AcceptableRotations;

	}
	
	void Update ()
	{
		
		Bounds mBounds = GetComponent<MeshFilter> ().mesh.bounds;

		if (toMove) {
			cf.force = (Destination - transform.position).normalized * MoveForce; 
		}
		if ((Destination - transform.position).sqrMagnitude < MoveAccuracy * MoveAccuracy) {
			toMove = false;
		}

		// depreciated
		if (toCombine && CombineTarget != null) {
			cf.force = ((CombineTarget.transform.TransformDirection (AnchorPoint) + CombineTarget.transform.position) - transform.position).normalized * MoveForce;

			if (((CombineTarget.transform.TransformDirection (AnchorPoint) + CombineTarget.transform.position) - transform.position).sqrMagnitude < 1f * 1f) {
				transform.SetParent (CombineTarget.transform);
				toCombine = false;
			}
		}

		if (toCombineAnchor && AnchorTarget != null) {
			
			RaycastHit raycastInfo;
			Ray toAnchor = new Ray (transform.position, AnchorTarget.transform.position - transform.position);
			//Debug.DrawLine (transform.position, (AnchorTarget.transform.position - transform.position).normalized * 2f + transform.position, Color.cyan, 1);
			bool hit = Physics.Raycast (toAnchor, out raycastInfo, 2);
			

			FindOutsideAnchor (ref raycastInfo, ref hit);

			if (hit && (raycastInfo.point - AnchorTarget.transform.position).magnitude > 0.25f) {
				Vector3 PreForce = (AnchorTarget.transform.position - transform.position).normalized * MoveForce *
				                   1f / Mathf.Pow ((AnchorTarget.transform.position - transform.position).magnitude, 0.5f);
				Vector3 SlideDirection = (PreForce - Vector3.Project (PreForce, raycastInfo.normal)).normalized;

				cf.force = PreForce.magnitude * SlideDirection; 
			} else {
				//print (cf.name + " " + cf.force.ToString ());
				//print (AnchorTarget.name + " " + AnchorTarget.transform.position.ToString ());
				cf.force = (AnchorTarget.transform.position - transform.position).normalized * MoveForce *
				1f / Mathf.Pow ((AnchorTarget.transform.position - transform.position).magnitude, 0.5f);
			}

			if ((AnchorTarget.transform.position - transform.position).sqrMagnitude < 0.3f * transform.lossyScale.x * transform.lossyScale.x
			    || (CompositeBlock && (AnchorTarget.transform.position - transform.position).sqrMagnitude < 0.1f * transform.lossyScale.x * transform.lossyScale.x * mBounds.size.sqrMagnitude)) {

				transform.SetParent (AnchorTarget.transform.parent);
				toCombineAnchor = false;
				cf.force = Vector3.zero;
				rb.constraints = RigidbodyConstraints.FreezePosition;

				transform.localRotation = SnapToRotationFace (transform.localRotation, AnchorNormal);

				if (CompositeBlock) {
					transform.position += (AnchorTarget.transform.position - transform.position) * 0.3f;
				}
				Controller.controller.MakeIntoComponent (AnchorTarget.transform.parent.gameObject);
			
				Controller.controller.SwitchToRootTexture (gameObject);

				BlockScript Papa = transform.root.GetComponent<BlockScript> ();
				Papa.PowerVal += PowerVal;
				Papa.EmitterVal += EmitterVal;
				Papa.DataVal += DataVal;

				LevelManager.levelManager.CheckIfSolved ();
				
			}
		}


		/*if (rb.velocity.sqrMagnitude > VelocityCap * VelocityCap) {
			rb.velocity = rb.velocity.normalized * VelocityCap;
		}*/

	}

	void FindOutsideAnchor (ref RaycastHit rh, ref bool DidHit)
	{
		if (DidHit) {
			if (rh.collider.transform.root == transform) {

				if (rh.collider.transform == transform) {
					print ("hitself");
				} else {
                    //raycast until outside
					Ray tooAnchor = new Ray (rh.collider.transform.position, AnchorTarget.transform.position - transform.position);
					Debug.DrawLine (rh.collider.transform.position, (AnchorTarget.transform.position - rh.collider.transform.position).normalized * 2f
					+ rh.collider.transform.position, Color.red, 1);

					DidHit = Physics.Raycast (tooAnchor, out rh, 2);

					FindOutsideAnchor (ref rh, ref DidHit);
				}
			}
		} else {
			return;
		}
	}

	Quaternion SnapToRotation (Quaternion initialRotation)
	{
		float lowestAngle = 360;
		Quaternion selectedRotation = Quaternion.identity;

		foreach (Quaternion AR in AcceptableRotations) {
			float angle1 = Quaternion.Angle (transform.localRotation, AR);
			if (angle1 < lowestAngle) {
				lowestAngle = angle1;
				selectedRotation = AR;
			}
		}

		return selectedRotation;

	}

	Quaternion SnapToRotationFace (Quaternion initialRotation, Vector3 normalF)
	{
		float lowestAngle = 360;
		Quaternion selectedRotation = Quaternion.identity;

		foreach (Quaternion AR in AcceptableRotations) {
			Quaternion Augmented = Quaternion.FromToRotation (Vector3.up, normalF) * AR;
			float angle1 = Quaternion.Angle (transform.localRotation, Augmented);
			if (angle1 < lowestAngle) {
				lowestAngle = angle1;
				selectedRotation = Augmented;
				print (" lowest angle = " + lowestAngle.ToString ());
			}
		}

		return selectedRotation;

	}
    //for targeting walls
	public void CombineTo (GameObject target, Vector3 clickPoint)
	{
		toMove = false;	

		toCombine = true;
		CombineTarget = target;
		Vector3 localToClickPoint = clickPoint - target.transform.position;
		AnchorPoint = target.transform.InverseTransformDirection (localToClickPoint);

		print ("combine " + gameObject.name + " with " + CombineTarget.name);
	}
    // for targeting blocks
	public void CombineToAnchor (GameObject target, Vector3 Normal)
	{
		transform.SetParent (null);

		rb.constraints = RigidbodyConstraints.None;

		toMove = false;	

		toCombine = false;

		//toCombineAnchor = true;

		AnchorTarget = target;

		AnchorNormal = Normal; //Relative to the target Object

		toCombineAnchor = true;

		////
		//TextBubble.TypeOut2 ();
		//TextBubble.TypeOut ("OK");
	}

    // finds the normal of a second adjacent edge on the target object
	public Vector3 FindSecondNormal (Transform ROOT, Vector3 Normal1, GameObject Anchor1)
	{
		RaycastHit hitInfo = new RaycastHit ();

		Vector3 randomVector = Random.onUnitSphere;
		Vector3 randPerp = Vector3.ProjectOnPlane (randomVector, Normal1);
		randPerp = randPerp.normalized;

		Vector3 RelativeNormal = Vector3.zero;

		float SubdivideLevel = 0.1f;
		float looseIndex = 0f;
		while (looseIndex < ROOT.lossyScale.x) {
			Ray ray = new Ray (ROOT.TransformDirection (-Normal1 * 0.3f + randPerp * looseIndex) + Anchor1.transform.position,
				          ROOT.TransformDirection (-randPerp));
			Debug.DrawRay (ROOT.TransformDirection (-Normal1 * 0.3f + randPerp * looseIndex) + Anchor1.transform.position,
				ROOT.TransformDirection (-randPerp), Color.red, 0.5f, false);

			if (Physics.Raycast (ray, out hitInfo, looseIndex)) {
				RelativeNormal = ROOT.InverseTransformDirection (hitInfo.normal);
				break;
			}
			looseIndex += SubdivideLevel;
		}
		
		Debug.DrawRay (hitInfo.point, ROOT.TransformDirection (RelativeNormal), Color.magenta, 2f, false);

		Vector3 answer = Vector3.ProjectOnPlane (RelativeNormal, Normal1).normalized;
		return answer;

	}

}
