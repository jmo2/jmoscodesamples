This is the block controller script for my game Archio.
It controls the movement and attaching of blocks to other blocks.
Taking an anchor point on another block, or object, it floats the
block towards that location and attempts to set the face of the block on
the face of the other object. 

The two things I like most about this script:

the recursive function"FindOutsideAnchor" that solves the problem of the block raycasting into its own sub-colliders,

it causes the block to move around an object if the target face is blocked
by the target object.


it has references to three scripts not included: AlienTextBubble.cs
Controller.cs, and LevelManager.cs. 

You can check out the game on my itch.io page, its free,
also I encluded a video.

https://capt-nemo.itch.io/personal-space-boy


 